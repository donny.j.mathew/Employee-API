package com.code.employeedbmanager.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GroupByDistrictResponse {
    private String district;
    private Long totalNoOfEmployees;
}
