package com.code.employeedbmanager.models;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Salary {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long sid;
    private String month;
    private String year;
    private float basicPay;
    private float bonus;
    private float hra;
    private float total;

}
