package com.code.employeedbmanager.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @NotNull
    private String houseNo;
    private String streetName;
    @NotNull
    private String district;
    @NotNull
    private String state;
    @NotNull
    private String country;
    @NotNull
    @Size(min = 6, message = "Pincode should have atleast 6 number")
    private String pinCode;
}
