package com.code.employeedbmanager.models;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @NotNull
    @Size(min = 2, message = "First Name should have atleast 2 characters")
    private String firstName;
    @Size(min = 1, message = "Last Name should have atleast 1 character")
    private String lastName;
    @NotNull
    @Positive
    private int age;
    @NotNull
    private String gender;
    @NotNull
    private String contactNumber;
    private String emergencyContactNumber;
    @OneToOne(targetEntity = Address.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "emp_id", referencedColumnName = "id")
    private Address currentAddress;
    @OneToMany(targetEntity = Salary.class, cascade=CascadeType.ALL)
    @JoinColumn(name = "emp_id", referencedColumnName = "id")
    private List<Salary> salary;
    @OneToMany(targetEntity = Leaves.class, cascade=CascadeType.ALL)
    @JoinColumn(name = "emp_id", referencedColumnName = "id")
    private List<Leaves> leaves;


}
