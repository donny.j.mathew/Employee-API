package com.code.employeedbmanager;

import com.code.employeedbmanager.repo.EmployeeRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackageClasses = EmployeeRepository.class)
@EntityScan("com.code.employeedbmanager.models")
public class EmployeeDbManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeDbManagerApplication.class, args);
	}


}
