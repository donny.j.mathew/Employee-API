package com.code.employeedbmanager.service;

import com.code.employeedbmanager.dto.GroupByDistrictResponse;
import com.code.employeedbmanager.exception.EmployeeNotFoundException;
import com.code.employeedbmanager.models.Employee;
import com.code.employeedbmanager.repo.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class EmployeeService {

    @Autowired
    EmployeeRepository employeeRepository;

    public List<Employee> getAllEmployees(){
        return employeeRepository.findAll();
    }

    public Employee getEmployeeById(Long id) throws EmployeeNotFoundException{
        return employeeRepository.findById(id)
                .orElseThrow(() -> new EmployeeNotFoundException("Employee not found with id: "+id));
    }

    public Employee addEmployee(Employee employee){
        return employeeRepository.save(employee);
    }

    public List<Employee> getEmployeeListByState(String state) throws EmployeeNotFoundException {
        List<Employee> employees = employeeRepository.getEmployeesByState(state);
        if(employees.isEmpty())
            throw new EmployeeNotFoundException("empty list of employees in the state: "+state);
        else
            return employeeRepository.getEmployeesByState(state);
    }

    public Map<String, Integer> countOfEmployeesInPinCode(String pinCode){
        Integer count = employeeRepository.getTotalEmployeesInPinCode(pinCode);
        Map<String, Integer> countOfEmployees = new HashMap();
        countOfEmployees.put("employeeCount", count);
        return countOfEmployees;
    }

    public List<GroupByDistrictResponse> getCountOfEmployeesByDistrict(){
        return employeeRepository.getCountOfEmployeesByDistrict();
    }

    public Employee getEmployeeHighestSalaryForMonth(String month) throws EmployeeNotFoundException {
        return Optional.ofNullable(employeeRepository.getEmployeeHighestSalaryForMonth(month))
                .orElseThrow(() -> new EmployeeNotFoundException("Empty employees for the month "+month));
    }

    public Employee getEmployeeHighestSalaryForYear(String year) throws EmployeeNotFoundException {
        return Optional.ofNullable(employeeRepository.getEmployeeHighestSalaryForYear(year))
                .orElseThrow(() -> new EmployeeNotFoundException("Empty employees for the year "+year));
    }

    public Employee findByDistrict(){
        return employeeRepository.findByCurrentAddressDistrict("kottayam");
    }
}
