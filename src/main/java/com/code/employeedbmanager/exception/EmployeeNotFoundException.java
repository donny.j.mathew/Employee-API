package com.code.employeedbmanager.exception;

import java.util.function.Supplier;

public class EmployeeNotFoundException extends Exception {
    public EmployeeNotFoundException(String message) {
        super(message);
    }
}
