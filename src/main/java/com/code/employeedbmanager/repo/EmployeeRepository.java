package com.code.employeedbmanager.repo;

import com.code.employeedbmanager.dto.GroupByDistrictResponse;
import com.code.employeedbmanager.models.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    @Query("select e from Employee e " +
            "join e.currentAddress a where a.state = :state")
    public List<Employee> getEmployeesByState(@Param("state") String state);

    @Query("select count(e) from Employee e join e.currentAddress a where a.pinCode = :pinCode")
    public Integer getTotalEmployeesInPinCode(@Param("pinCode") String pinCode);

    @Query("select new com.code.employeedbmanager.dto.GroupByDistrictResponse(a.district, count(a)) " +
            "from Address a group by a.district")
    public List<GroupByDistrictResponse> getCountOfEmployeesByDistrict();

    @Query("select e from Employee e join e.salary s " +
            "where s.total = (select max(s.total) from Salary s where s.month = :month) and s.month = :month")
    public Employee getEmployeeHighestSalaryForMonth(@Param("month") String month);

    @Query("select e from Employee e join e.salary s " +
            "where s.total = (select max(s.total) from Salary s where s.year = :year) and s.year = :year")
    public Employee getEmployeeHighestSalaryForYear(@Param("year") String year);

    public Employee findByCurrentAddressDistrict(String district);
}
