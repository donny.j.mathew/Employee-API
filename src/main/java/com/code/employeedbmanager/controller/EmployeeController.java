package com.code.employeedbmanager.controller;

import com.code.employeedbmanager.dto.GroupByDistrictResponse;
import com.code.employeedbmanager.exception.EmployeeNotFoundException;
import com.code.employeedbmanager.service.EmployeeService;
import com.code.employeedbmanager.models.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/employees")
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;

    @PostMapping
    public Employee newEmployee(@Valid @RequestBody Employee employee){
        return employeeService.addEmployee(employee);
    }

    @GetMapping
    public ResponseEntity<List<Employee>> getAll(){
        return new ResponseEntity<>(employeeService.getAllEmployees(), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Employee> getById(@PathVariable(required = true) Long id) throws EmployeeNotFoundException {
        return new ResponseEntity<>(employeeService.getEmployeeById(id), HttpStatus.OK);
    }

    @GetMapping(value = "/state/{state}")
    public ResponseEntity<List<Employee>> getEmployeesByState(@PathVariable(required = true) String state) throws EmployeeNotFoundException {
        return new ResponseEntity<>(employeeService.getEmployeeListByState(state), HttpStatus.OK);
    }

    @GetMapping(value = "/pincode/{pinCode}")
    public ResponseEntity<Map<String, Integer>> countOfEmployeesInPinCode(@PathVariable(required = true) String pinCode){
        return new ResponseEntity<>(employeeService.countOfEmployeesInPinCode(pinCode), HttpStatus.OK);
    }

    @GetMapping(value = "/districts")
    public ResponseEntity<List<GroupByDistrictResponse>> groupByDistricts(){
        return new ResponseEntity<>(employeeService.getCountOfEmployeesByDistrict(), HttpStatus.OK);
    }

    @GetMapping(value = "salary/month/{month}")
    public ResponseEntity<Employee> highestPaidEmployeeInMonth(@PathVariable(required = true) String month) throws EmployeeNotFoundException {
        return new ResponseEntity<>(employeeService.getEmployeeHighestSalaryForMonth(month), HttpStatus.OK);
    }

    @GetMapping(value = "salary/year/{year}")
    public ResponseEntity<Employee> highestPaidEmployeeInYear(@PathVariable(required = true) String year) throws EmployeeNotFoundException {
        return new ResponseEntity<>(employeeService.getEmployeeHighestSalaryForYear(year), HttpStatus.OK);
    }

    @GetMapping(value = "demo")
    public ResponseEntity<Employee> findBydistrict() {
        return new ResponseEntity<>(employeeService.findByDistrict(), HttpStatus.OK);
    }
}
