FROM openjdk:11
MAINTAINER donnyj

EXPOSE 8080

WORKDIR /usr/app
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} employee-directory.jar

ENTRYPOINT ["java", "-jar", "employee-directory.jar"]
